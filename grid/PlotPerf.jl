using Plots 
using customPPN
pgfplotsx();

 function plotRes(struc)
 	mat = struc.nbE ./ struc.nbC; 
 	matPPN = struc.nbEPPN ./ struc.nbCPPN; 
 	p = plot(tex_output_standalone = true, size = (400, 300))
 	for (iτ, τ) ∈ enumerate(struc.τVect)
     τ= struc.τVect[iτ];
 		for (iλ, λ) ∈ enumerate(struc.λVect)
            if iλ > 1 # Remove first lambda as 2 does not work for K=4
 			    plot!(p, struc.snrVect, 1e-10 .+ mat[iτ,iλ,:], marker = getMarker(iτ), color = getColorLatex(iλ), linestyle=:solid,label = "FFT - $λ - $τ",yaxis=:log);
 			    plot!(p, struc.snrVect, 1e-10 .+ matPPN[iτ,iλ,:], marker = getMarker(iτ), color = getColorLatex(iλ), linestyle=:dash,label = "PPN - λ = $λ- τ =  $τ %",yaxis=:log);
            end
 		end
 	end
 	ylims!(p,1e-5,1);
 	xlabel!(p, "SNR [dB]");
 	ylabel!(p, "CER")
 	display(p)
end

 function plotRes(nbE, nbC, fftVect, snrV)
 	mat = nbC ./ nbE; 
 	p = plot(tex_output_standalone = true, size = (400, 300));
 	plot!(p, snrV, 1e-8 .+ mat, marker = reshape([getMarker(i) for i = 1:1:length(fftVect)], 1, :), label = transpose(fftVect), yaxis = :log)
 	xlabel!("SNR [dB]");
 	ylabel!("CER");
 	ylims!(p, (1e-6, 1));
 	display(p);
 	return p;
 end
 function getMarker(ind)
 	dict = [:circle,:diamond,:dtriangle,:pentagon,:rect,:star,:utriangle,:vline,:xcross,:hline];
 	ll = length(dict)
 	return dict[mod(ind - 1, ll) + 1];
 end

 """ 
 --- 
 Get Latex color for PGF plotds 
 --- Syntax 
 c = getColorLatex(ind,size=1)
 # --- Input parameters 
 - ind	  : Desired color index [Int]
 - Size	  : Number of color to be generated (necessary ind ind > 5)
 # --- Output parameters 
 - a		  : RGB sequence [Char]
 # --- 
 # v 1.0 - Robin Gerzaguet.
 """
 function getColorLatex(index, size = 1);
 	if index > 5 
 		size = index;
 	end
 	if size < 5 
 		# ---------------------------------------------------- 
 		# --- Manual color generation 
 		# ---------------------------------------------------- 
 		c	= ();
 		# --- Manually create colors 
 		# c	= (c..., "rgb,1:red,0; green,0.44700; blue,0.74100");
 		c	= (c..., RGB(0,0.447,0.74100));
 		c	= (c..., "orange");
 		c	= (c..., "violet");
 		c	= (c..., "darkgreen");
 		c	= (c..., "black");
 		col  = c[index];
 	else 
 		# ---------------------------------------------------- 
 		# --- Automatic color generation 
 		# ---------------------------------------------------- 
 		col = distinguishable_colors(size);#
 		r	= Int(floor(col.r * 255));
 		b	= Int(floor(col.b * 255));
 		g	= Int(floor(col.g * 255));
 		# col = "rgb,255:red,$r; green,$g; blue,$b";
 		col = RGB(r,b,g);
 	end 
 	return col;
 end
