#!/bin/bash
#OAR -l /nodes=1/core=16,walltime=150:00:00

# --- We are sure to load profile 
source $HOME/.profile			  # Ensure that ENV is updated (SCRATCHDIR def)
source /etc/profile.d/modules.sh  # file for module autoload 
module load julia/1.2.0			  # Loading module of interest

# --- Simple print of current deployment
echo "My job was ran on these nodes:"
cat $OAR_NODEFILE

# --- Main processing call
julia --project -p 16 grid/launch_igrida.jl

