

# be sure that julia is launched as followd 
# julia -p N simulations/ChannelOnlyCER/launch_igrida.jl 
# Number of core are in batch.sh no need to redoing stuff here
#  oarsub -S $HOME/Documents/tempest_fh/batch.s 

#@show ENV["SCRATCHDIR"]

# --- Define MODULE of interest
using Distributed 

@info "We are in the Julia part, ready to compile the function";
using Distributed, Pkg
@everywhere using Distributed, Pkg
Pkg.activate(".")
@everywhere Pkg.activate(".")
using customPPN
@everywhere using customPPN


@info "Now, we launch the main call. Ready !"
res = customPPN.benchDistributed();

# --- Saving structures
import Pkg; Pkg.add("JLD2")
using JLD2
rr = ENV["SCRATCHDIR"]
@save  "$rr/perf_delay.jld2" res





# --- result should be on 
# julia> ENV["SCRATCHDIR"]
# "/temp_dd/igrida-fs1/rgerzagu/SCRATCH"
