module customPPN

# Write your package code here.
using DSP 
using FFTW 
using DigitalComm

using Distributed 
export tx;
export fftDetector
export ppnDetector
export bench 

export Res

include("Bench.jl")

function tx(duration=0.01,N=80,λ=60)
    # ----------------------------------------------------
    # --- Parameters
    # ---------------------------------------------------- 
    # duration    = 0.01;
    # N           = 80;
    # λ           = 60;
    blackRate   = 1e6;
    freqTones   = [700;900;1200;700];
    samplingRate= 80e6;
    burstDuration = N * λ / samplingRate;
    nbHops     = 1+Int(duration ÷ burstDuration);
    # ----------------------------------------------------
    # --- Generation of FH
    # ---------------------------------------------------- 
    nbSymb      = Int(floor(duration * samplingRate / length(freqTones))*length(freqTones));
    # --- Create sequence
	sequence = rand(1:N, nbHops);
	# --- Create time domain signal 
    d   = [exp.(2im * π / N * ind * n) for ind ∈ sequence for n ∈ (0:λ * N - 1)];
    # ----------------------------------------------------
    # --- Generation of tones 
    # ---------------------------------------------------- 
    durTones    = Int(ceil(length(d) / length(freqTones)));
    c   = [1+exp.(2im * π *freq / samplingRate * n) for freq ∈ freqTones for n ∈ (0:durTones-1)];
    # ----------------------------------------------------
    # --- Mixing
    # ---------------------------------------------------- 
    return d,sequence
end

function fftDetector(x::Vector{T},N,λ) where T
    p̂ = Vector{Int64}(undef,0)
    nbS = length(x) ÷ (λ * N) 
    offS = 0;
    for n ∈ 1 : nbS 
        y = zeros(T,N);
        for l ∈ 1 : λ 
            X = x[ offS .+ (1:N)];
            y += fft(X);
            offS += N;
        end
        pos = mod(argmax(abs2.(y)) - 1, N);
        (pos == 0) && (pos = 80);
        push!(p̂,pos);
    end
    return p̂
end

"""
Detector based on PPN
"""
function ppnDetector(x::Vector{T},N,λ,K) where T
    # --- Define Filter 
    h = getFBMCFilter(K,N);
    # --- Define parameters 
    p̂ = Vector{Int64}(undef,0)
    # --- Number of blocks on which we do a PPN (frame of λN)
    nbS = length(x) ÷ (λ * N) 
    # --- Number of overlapped bins in each frame 
    # We have N overlaped so last point should verify αN + KN = λN 
    α = λ - K; 
    for n ∈ 1 : nbS - 1
        z   = zeros(T,N); 
        subX = x[ (n-1)*λ*N .+ (1:λ*N)];
        for a ∈ 1 : α 
            # --- KN buffer on which we do the PPN 
            subsubX = subX[ (a-1)*N .+ (1:K*N)] ;
            # --- We do the FFT 
            y = fft(subsubX);
            # --- Frequency unspreading 
            z += ppnGather(y,K);
        end
        pos = mod(argmax(abs2.(z)) , N);
        (pos == 0) && (pos = 80);
        push!(p̂,pos);
    end
    return p̂
end

"""
Creates coefficient of filter 
"""
function getCoeff(K)
    # --- Coefficient of filter in freq domain, based here on Phydias template
    if K == 4
        p1			= 0.97195983;
        p2			= 1/sqrt(2);
        p3          = sqrt(1-p1^2);
        P			= [p1;p2;p3];
        return [P[end:-1:1];1;P]
    end
end

"""
From oversamp filter, returns the unspread spectrum 
See "Optimization of Hopping DFT for FS-FBMC Receivers", HusamAl-amaireh Zsolt Kollár, Signal Processing, 2021
"""
function ppnGather(y::Vector{T},K) where T
    # --- Get coefficients in freq domain 
    c = getCoeff(K);
    # --- FFT size 
    N = length(y) ÷ K;
    # --- Spreading area 
    A = -K+1 : K-1;
    # --- Output init 
    z = zeros( T, N);
    for n ∈ 1 : N 
        for (k,a) ∈ enumerate(A)
                z[n] +=  c[k] * y[1 + mod(K*n + a,K*N)];
        end
    end
    return z;
end

function bench(N,λ,K)
    snr = (-20:20);
    nbSNR = length(snr);
    nbChanFFT = zeros(Int64,nbSNR);
    nbErrFFT  = zeros(Int64,nbSNR);
    nbChanPPN = zeros(Int64,nbSNR);
    nbErrPPN  = zeros(Int64,nbSNR);
    nbMC = 100;
    for iSnr = 1 : 1 : nbSNR 
        for iMC = 1 : 1 : nbMC 
            d,p = tx(0.01,N,λ);
            r,_  = addNoise(d,snr[iSnr]);
            pFFT = fftDetector(r,N,λ);
            pPPN = ppnDetector(r,N,λ,K);
            nbErrFFT[iSnr] +=  sum( p .!== pFFT);
            nbChanFFT[iSnr] += length(p) ;
            nbErrPPN[iSnr] +=  sum( p[1:end-1] .!== pPPN);
            nbChanPPN[iSnr] += length(p) - 1;
        end
        println("done");
    end
    return (N,λ,K,snr,nbErrFFT ./ nbChanFFT, nbErrPPN ./ nbChanPPN)
end
            

end
