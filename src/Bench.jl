using Distributed
export coreProcessing
@everywhere begin 
	using DigitalComm
	using FFTW
end


struct Res
	N::Int                          # Number of Channels 
    λVect::AbstractArray;           # Vector of repeating factor 
	τVect::AbstractArray;           # Vector of delays
	snrVect::AbstractArray;         # Vector of SNR 
	nbIt::Int;                      # Number of MC runs 
	nbE::AbstractArray;             # Number of errors, FFT
	nbC::AbstractArray;             # Number of detections, FFT
	nbEPPN::AbstractArray;          # Number of errors, PPN 
	nbCPPN::AbstractArray;          # Number of detection, PPN
end

function coreProcessing(iSnr, iτ, iλ, N, L, λ, τ, snr)
	# --- Create sequence
	sequence = rand(1:N, L);
	# --- Create time domain signal 
	d   = [exp.(2im * π / N * ind * n) for ind ∈ sequence for n ∈ (0:λ * N - 1)];
	# --- Add delay 
    if τ !== 0
	    k   = round(τ / 100 * λ * N);
	    d   = circshift(d, k);
    end
	# --- Add noise 
	d,   = addNoise(d, snr);
	# --- Detector 
	p       =  fftDetector(d,N,λ)
	pP       =  ppnDetector(d,N,λ,4)
	# Populate matrixes 
	nbE = sum(p .!= sequence);
	nbEP = sum(pP .!= sequence[1:end-1]);
	# --- Return
	return (iSnr, iτ, iλ, L, nbE,nbEP);
end



function benchDistributed()
	#
	N       = 80;
	L       = 500;
	#
	λVect   = [2;20;200;625];
	τVect   = [0;1;15;25];
	snrVect    = (-45:0);
	nbIt    = 100;
	#	
	nbE     = zeros(Int, length(τVect), length(λVect), length(snrVect));
	nbEPPN     = zeros(Int, length(τVect), length(λVect), length(snrVect));
	nbC     = zeros(Int, length(τVect), length(λVect), length(snrVect));
	nbCPPN     = zeros(Int, length(τVect), length(λVect), length(snrVect));
	tS      = [];
	for (iSnr, snr) ∈ enumerate(snrVect)
		for (iλ, λ) ∈ enumerate(λVect)
			for it ∈ (1:nbIt)
				for (iτ, τ) ∈ enumerate(τVect)    
					cS = @spawn coreProcessing(iSnr, iτ, iλ, N, L, λ, τ, snr);
					push!(tS, cS);
				end
			end
		end
	end
	# --- Waiting for all results to be done 
	foreach(wait, tS);
	@info "Simulation done"
	# --- Populating data 
	for t in tS 
		(iSnr, iτ, iλ, L, err,errPPN)  = fetch(t);
		nbC[iτ,iλ,iSnr] += L;
		nbCPPN[iτ,iλ,iSnr] += L - 1;
		nbE[iτ,iλ,iSnr] += err;
		nbEPPN[iτ,iλ,iSnr] += errPPN;
	end
	return Res(N, λVect, τVect, snrVect, nbIt, nbE, nbC,nbEPPN,nbCPPN) 
end

