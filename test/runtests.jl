using customPPN
using Test

@testset "Checking FFT detector" begin
    d,p = tx();
    p̂ = fftDetector(d,80,60);
    @test all(p .== p̂) == true
end


@testset "Checking PPN detector" begin
    d,p = tx();
    p̂ = ppnDetector(d,80,60,4);
    @test all(p[1:end-1] .== p̂) == true
end
